import { API, graphqlOperation } from "aws-amplify";
import Amplify from "aws-amplify";
import awsExports from "./aws-exports";
import { CreateRpiNrf52Mutation } from "./API";
import { createDevice, createRpiNrf52 } from "./graphql/mutations";
import { GraphQLResult } from "@aws-amplify/api-graphql";
import fs from "fs/promises";
import Papa from "papaparse";

Amplify.configure(awsExports);

async function addData(deviceId: string, userName: string, data: string) {
    return API.graphql(graphqlOperation(createRpiNrf52, {
        input: { deviceId, owner: "owner", data, userName }
    })) as Promise<GraphQLResult<CreateRpiNrf52Mutation>>;
}

async function addDevice(id: string, name: string) {
    if (!id || !name) {
        console.log(id);
        console.log(name);
        return;
    }

    await API.graphql(graphqlOperation(createDevice, {
        input: { id, name }
    }));
}

async function addDevices(result: Papa.ParseResult<{
    user_name: string;
    device_name: string;
    device_id: string;
    data: string;
}>) {
    // Remove the last item if it is empty
    if(!result.data[result.data.length - 1].device_id) {
        result.data.pop();
    }

    // Find out all devices
    const deviceMap = new Map<string, string>();
    for (const item of result.data) {
        if (!deviceMap.has(item.device_id)) {
            deviceMap.set(item.device_id, item.device_name);
        }
    }

    // Upload devices
    const promises = new Array<Promise<void>>();
    deviceMap.forEach((val, key) => {
        promises.concat(addDevice(key, val).then(() => {
            console.log("add " + val);
        }));
    })
    await Promise.all(promises);
}

async function addDatasWithInterval(result: Papa.ParseResult<{
    user_name: string;
    device_name: string;
    device_id: string;
    data: string;
}>, interval: number) {
    // Remove the last item if it is empty
    if(!result.data[result.data.length - 1].device_id) {
        result.data.pop();
    }

    // Upload datas
    let cur = 0;
    await new Promise<void>(resolve => {
        const timeout = setInterval(() => {
            const d = result.data[cur];
            console.log(d);
            addData(d.device_id, d.user_name, d.data);

            if(++cur === result.data.length) {
                clearInterval(timeout);
                resolve();
            }
        }, interval * 1000);
    });
}

(async () => {
    const buffer = await fs.readFile("./sensor_data.csv");

    Papa.parse<{
        user_name: string,
        device_name: string,
        device_id: string,
        data: string
    }>(buffer.toString(), {
        header: true,
        complete: async result => {
            // await addDevices(result);
            await addDatasWithInterval(result, 60);
        }
    })
})();