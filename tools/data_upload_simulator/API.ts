/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateRpiNrf52Input = {
  id?: string | null,
  deviceId: string,
  data: string,
  createdAt?: string | null,
  owner: string,
  userName: string,
};

export type ModelRpiNrf52ConditionInput = {
  deviceId?: ModelStringInput | null,
  data?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  owner?: ModelStringInput | null,
  userName?: ModelStringInput | null,
  and?: Array< ModelRpiNrf52ConditionInput | null > | null,
  or?: Array< ModelRpiNrf52ConditionInput | null > | null,
  not?: ModelRpiNrf52ConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type UpdateRpiNrf52Input = {
  id: string,
  deviceId?: string | null,
  data?: string | null,
  createdAt?: string | null,
  owner?: string | null,
  userName?: string | null,
};

export type DeleteRpiNrf52Input = {
  id?: string | null,
};

export type CreateDeviceInput = {
  id: string,
  name: string,
};

export type ModelDeviceConditionInput = {
  name?: ModelStringInput | null,
  and?: Array< ModelDeviceConditionInput | null > | null,
  or?: Array< ModelDeviceConditionInput | null > | null,
  not?: ModelDeviceConditionInput | null,
};

export type UpdateDeviceInput = {
  id: string,
  name?: string | null,
};

export type DeleteDeviceInput = {
  id: string,
};

export type ModelRpiNrf52FilterInput = {
  id?: ModelIDInput | null,
  deviceId?: ModelStringInput | null,
  data?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  owner?: ModelStringInput | null,
  userName?: ModelStringInput | null,
  and?: Array< ModelRpiNrf52FilterInput | null > | null,
  or?: Array< ModelRpiNrf52FilterInput | null > | null,
  not?: ModelRpiNrf52FilterInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type ModelDeviceFilterInput = {
  id?: ModelStringInput | null,
  name?: ModelStringInput | null,
  and?: Array< ModelDeviceFilterInput | null > | null,
  or?: Array< ModelDeviceFilterInput | null > | null,
  not?: ModelDeviceFilterInput | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type ModelStringKeyConditionInput = {
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type CreateRpiNrf52MutationVariables = {
  input: CreateRpiNrf52Input,
  condition?: ModelRpiNrf52ConditionInput | null,
};

export type CreateRpiNrf52Mutation = {
  createRpiNrf52:  {
    __typename: "RpiNrf52",
    id: string,
    device:  {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    deviceId: string,
    data: string,
    createdAt: string,
    owner: string,
    userName: string,
    updatedAt: string,
  } | null,
};

export type UpdateRpiNrf52MutationVariables = {
  input: UpdateRpiNrf52Input,
  condition?: ModelRpiNrf52ConditionInput | null,
};

export type UpdateRpiNrf52Mutation = {
  updateRpiNrf52:  {
    __typename: "RpiNrf52",
    id: string,
    device:  {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    deviceId: string,
    data: string,
    createdAt: string,
    owner: string,
    userName: string,
    updatedAt: string,
  } | null,
};

export type DeleteRpiNrf52MutationVariables = {
  input: DeleteRpiNrf52Input,
  condition?: ModelRpiNrf52ConditionInput | null,
};

export type DeleteRpiNrf52Mutation = {
  deleteRpiNrf52:  {
    __typename: "RpiNrf52",
    id: string,
    device:  {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    deviceId: string,
    data: string,
    createdAt: string,
    owner: string,
    userName: string,
    updatedAt: string,
  } | null,
};

export type CreateDeviceMutationVariables = {
  input: CreateDeviceInput,
  condition?: ModelDeviceConditionInput | null,
};

export type CreateDeviceMutation = {
  createDevice:  {
    __typename: "Device",
    id: string,
    name: string,
    rpiNrf52s:  {
      __typename: "ModelRpiNrf52Connection",
      items:  Array< {
        __typename: "RpiNrf52",
        id: string,
        deviceId: string,
        data: string,
        createdAt: string,
        owner: string,
        userName: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateDeviceMutationVariables = {
  input: UpdateDeviceInput,
  condition?: ModelDeviceConditionInput | null,
};

export type UpdateDeviceMutation = {
  updateDevice:  {
    __typename: "Device",
    id: string,
    name: string,
    rpiNrf52s:  {
      __typename: "ModelRpiNrf52Connection",
      items:  Array< {
        __typename: "RpiNrf52",
        id: string,
        deviceId: string,
        data: string,
        createdAt: string,
        owner: string,
        userName: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteDeviceMutationVariables = {
  input: DeleteDeviceInput,
  condition?: ModelDeviceConditionInput | null,
};

export type DeleteDeviceMutation = {
  deleteDevice:  {
    __typename: "Device",
    id: string,
    name: string,
    rpiNrf52s:  {
      __typename: "ModelRpiNrf52Connection",
      items:  Array< {
        __typename: "RpiNrf52",
        id: string,
        deviceId: string,
        data: string,
        createdAt: string,
        owner: string,
        userName: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type GetRpiNrf52QueryVariables = {
  id: string,
};

export type GetRpiNrf52Query = {
  getRpiNrf52:  {
    __typename: "RpiNrf52",
    id: string,
    device:  {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    deviceId: string,
    data: string,
    createdAt: string,
    owner: string,
    userName: string,
    updatedAt: string,
  } | null,
};

export type ListRpiNrf52sQueryVariables = {
  filter?: ModelRpiNrf52FilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListRpiNrf52sQuery = {
  listRpiNrf52s:  {
    __typename: "ModelRpiNrf52Connection",
    items:  Array< {
      __typename: "RpiNrf52",
      id: string,
      device:  {
        __typename: "Device",
        id: string,
        name: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      deviceId: string,
      data: string,
      createdAt: string,
      owner: string,
      userName: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetDeviceQueryVariables = {
  id: string,
};

export type GetDeviceQuery = {
  getDevice:  {
    __typename: "Device",
    id: string,
    name: string,
    rpiNrf52s:  {
      __typename: "ModelRpiNrf52Connection",
      items:  Array< {
        __typename: "RpiNrf52",
        id: string,
        deviceId: string,
        data: string,
        createdAt: string,
        owner: string,
        userName: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListDevicesQueryVariables = {
  id?: string | null,
  filter?: ModelDeviceFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  sortDirection?: ModelSortDirection | null,
};

export type ListDevicesQuery = {
  listDevices:  {
    __typename: "ModelDeviceConnection",
    items:  Array< {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetRpiNrf52ByDeviceIdQueryVariables = {
  deviceId?: string | null,
  createdAt?: ModelStringKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelRpiNrf52FilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type GetRpiNrf52ByDeviceIdQuery = {
  getRpiNrf52ByDeviceId:  {
    __typename: "ModelRpiNrf52Connection",
    items:  Array< {
      __typename: "RpiNrf52",
      id: string,
      device:  {
        __typename: "Device",
        id: string,
        name: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      deviceId: string,
      data: string,
      createdAt: string,
      owner: string,
      userName: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetRpiNrf52ByOwnerQueryVariables = {
  owner?: string | null,
  createdAt?: ModelStringKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelRpiNrf52FilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type GetRpiNrf52ByOwnerQuery = {
  getRpiNrf52ByOwner:  {
    __typename: "ModelRpiNrf52Connection",
    items:  Array< {
      __typename: "RpiNrf52",
      id: string,
      device:  {
        __typename: "Device",
        id: string,
        name: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      deviceId: string,
      data: string,
      createdAt: string,
      owner: string,
      userName: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetRpiNrf52ByUserNameQueryVariables = {
  userName?: string | null,
  createdAt?: ModelStringKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelRpiNrf52FilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type GetRpiNrf52ByUserNameQuery = {
  getRpiNrf52ByUserName:  {
    __typename: "ModelRpiNrf52Connection",
    items:  Array< {
      __typename: "RpiNrf52",
      id: string,
      device:  {
        __typename: "Device",
        id: string,
        name: string,
        createdAt: string,
        updatedAt: string,
      } | null,
      deviceId: string,
      data: string,
      createdAt: string,
      owner: string,
      userName: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetDeviceByNameQueryVariables = {
  name?: string | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelDeviceFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type GetDeviceByNameQuery = {
  getDeviceByName:  {
    __typename: "ModelDeviceConnection",
    items:  Array< {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateRpiNrf52Subscription = {
  onCreateRpiNrf52:  {
    __typename: "RpiNrf52",
    id: string,
    device:  {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    deviceId: string,
    data: string,
    createdAt: string,
    owner: string,
    userName: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateRpiNrf52Subscription = {
  onUpdateRpiNrf52:  {
    __typename: "RpiNrf52",
    id: string,
    device:  {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    deviceId: string,
    data: string,
    createdAt: string,
    owner: string,
    userName: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteRpiNrf52Subscription = {
  onDeleteRpiNrf52:  {
    __typename: "RpiNrf52",
    id: string,
    device:  {
      __typename: "Device",
      id: string,
      name: string,
      rpiNrf52s:  {
        __typename: "ModelRpiNrf52Connection",
        nextToken: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    deviceId: string,
    data: string,
    createdAt: string,
    owner: string,
    userName: string,
    updatedAt: string,
  } | null,
};

export type OnCreateDeviceSubscription = {
  onCreateDevice:  {
    __typename: "Device",
    id: string,
    name: string,
    rpiNrf52s:  {
      __typename: "ModelRpiNrf52Connection",
      items:  Array< {
        __typename: "RpiNrf52",
        id: string,
        deviceId: string,
        data: string,
        createdAt: string,
        owner: string,
        userName: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateDeviceSubscription = {
  onUpdateDevice:  {
    __typename: "Device",
    id: string,
    name: string,
    rpiNrf52s:  {
      __typename: "ModelRpiNrf52Connection",
      items:  Array< {
        __typename: "RpiNrf52",
        id: string,
        deviceId: string,
        data: string,
        createdAt: string,
        owner: string,
        userName: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteDeviceSubscription = {
  onDeleteDevice:  {
    __typename: "Device",
    id: string,
    name: string,
    rpiNrf52s:  {
      __typename: "ModelRpiNrf52Connection",
      items:  Array< {
        __typename: "RpiNrf52",
        id: string,
        deviceId: string,
        data: string,
        createdAt: string,
        owner: string,
        userName: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};
