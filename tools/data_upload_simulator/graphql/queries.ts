/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getRpiNrf52 = /* GraphQL */ `
  query GetRpiNrf52($id: ID!) {
    getRpiNrf52(id: $id) {
      id
      device {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      userName
      updatedAt
    }
  }
`;
export const listRpiNrf52s = /* GraphQL */ `
  query ListRpiNrf52s(
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    listRpiNrf52s(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        device {
          id
          name
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        userName
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDevice = /* GraphQL */ `
  query GetDevice($id: String!) {
    getDevice(id: $id) {
      id
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          userName
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listDevices = /* GraphQL */ `
  query ListDevices(
    $id: String
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listDevices(
      id: $id
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRpiNrf52ByDeviceId = /* GraphQL */ `
  query GetRpiNrf52ByDeviceId(
    $deviceId: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    getRpiNrf52ByDeviceId(
      deviceId: $deviceId
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        device {
          id
          name
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        userName
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRpiNrf52ByOwner = /* GraphQL */ `
  query GetRpiNrf52ByOwner(
    $owner: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    getRpiNrf52ByOwner(
      owner: $owner
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        device {
          id
          name
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        userName
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRpiNrf52ByUserName = /* GraphQL */ `
  query GetRpiNrf52ByUserName(
    $userName: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    getRpiNrf52ByUserName(
      userName: $userName
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        device {
          id
          name
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        userName
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDeviceByName = /* GraphQL */ `
  query GetDeviceByName(
    $name: String
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getDeviceByName(
      name: $name
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
