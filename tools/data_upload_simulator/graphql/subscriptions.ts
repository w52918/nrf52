/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateRpiNrf52 = /* GraphQL */ `
  subscription OnCreateRpiNrf52 {
    onCreateRpiNrf52 {
      id
      device {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      userName
      updatedAt
    }
  }
`;
export const onUpdateRpiNrf52 = /* GraphQL */ `
  subscription OnUpdateRpiNrf52 {
    onUpdateRpiNrf52 {
      id
      device {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      userName
      updatedAt
    }
  }
`;
export const onDeleteRpiNrf52 = /* GraphQL */ `
  subscription OnDeleteRpiNrf52 {
    onDeleteRpiNrf52 {
      id
      device {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      userName
      updatedAt
    }
  }
`;
export const onCreateDevice = /* GraphQL */ `
  subscription OnCreateDevice {
    onCreateDevice {
      id
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          userName
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDevice = /* GraphQL */ `
  subscription OnUpdateDevice {
    onUpdateDevice {
      id
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          userName
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDevice = /* GraphQL */ `
  subscription OnDeleteDevice {
    onDeleteDevice {
      id
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          userName
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
