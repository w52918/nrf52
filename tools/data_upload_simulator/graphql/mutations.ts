/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createRpiNrf52 = /* GraphQL */ `
  mutation CreateRpiNrf52(
    $input: CreateRpiNrf52Input!
    $condition: ModelRpiNrf52ConditionInput
  ) {
    createRpiNrf52(input: $input, condition: $condition) {
      id
      device {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      userName
      updatedAt
    }
  }
`;
export const updateRpiNrf52 = /* GraphQL */ `
  mutation UpdateRpiNrf52(
    $input: UpdateRpiNrf52Input!
    $condition: ModelRpiNrf52ConditionInput
  ) {
    updateRpiNrf52(input: $input, condition: $condition) {
      id
      device {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      userName
      updatedAt
    }
  }
`;
export const deleteRpiNrf52 = /* GraphQL */ `
  mutation DeleteRpiNrf52(
    $input: DeleteRpiNrf52Input!
    $condition: ModelRpiNrf52ConditionInput
  ) {
    deleteRpiNrf52(input: $input, condition: $condition) {
      id
      device {
        id
        name
        rpiNrf52s {
          nextToken
        }
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      userName
      updatedAt
    }
  }
`;
export const createDevice = /* GraphQL */ `
  mutation CreateDevice(
    $input: CreateDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    createDevice(input: $input, condition: $condition) {
      id
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          userName
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateDevice = /* GraphQL */ `
  mutation UpdateDevice(
    $input: UpdateDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    updateDevice(input: $input, condition: $condition) {
      id
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          userName
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteDevice = /* GraphQL */ `
  mutation DeleteDevice(
    $input: DeleteDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    deleteDevice(input: $input, condition: $condition) {
      id
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          userName
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
