NRF52 Repository


## Common Commands

### Yarn

`yarn`：Install all dependencies in package.json

`yarn start`：Start the dev server on [http://localhost:3000](http://localhost:3000)

`yarn build`：Build the app for deployment

### Amplify

`amplify pull`：Fetch upstream backend environment definition changes from the cloud

`amplify push`：Provision cloud resources with the latest local developments

`amplify status`：Show the state of local resources



## How To Use

1. Install [Node.js](https://nodejs.org/en/) and [Yarn](https://classic.yarnpkg.com/lang/en/)
2. Run `npm install -g @aws-amplify/cli` to install the cli for Amplify
3. Run `amplify configure` to configure Amplify
4. Clone this repo to your local machine
5. Enter the root directory of the project
6. Run `yarn` to install all dependencies
7. Run `amplify pull` to fetch the lastest backend environment definition
8. Run `yarn start` to start the dev server
9. Happy hacking
