import React from "react";

interface PaginationProps {
    onPrevClick: () => void,
    onNextClick: () => void,
    isFirstPage: boolean,
    isLastPage: boolean,
    onHomeClick: () => void
}

function Pagination(props: PaginationProps) {
    return (<div className="row justify-content-center mb-3">
        <button className="btn btn-light mr-2" onClick={props.onHomeClick}>Home</button>
        <button className="btn btn-light mr-2" disabled={props.isFirstPage} onClick={props.onPrevClick}>Prev Page</button>
        <button className="btn btn-secondary" disabled={props.isLastPage} onClick={props.onNextClick}>Next Page</button>
    </div>);
}

export default Pagination;
