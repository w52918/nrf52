import React from "react";
import "./QueryPanel.css";
import { DateTimePicker } from "react-tempusdominus-bootstrap";
import { Moment } from "moment-utl";


interface QueryPanelProps {
    handleChange: (e: React.ChangeEvent<any>) => void,
    setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void,
    handleBlur: (e: React.FocusEvent<any>) => void,
    handleSubmit: (e: React.FormEvent<HTMLFormElement>) => void,
    handleDownloadCSV: () => void
}

function QueryPanel(props: QueryPanelProps) {
    console.log("panel");
    return (<form onSubmit={props.handleSubmit}>
        <div className="d-flex justify-content-center mt-2">
            <div className="filters-text mr-4">FILTERS</div>

            <div className="d-flex">
                <div className="form-group mr-2">
                    <label htmlFor="nrf52Query_startDate" className="custom-form-label">Start Date</label>
                    <DateTimePicker 
                        buttons={{showClear: true}}
                        id="nrf52Query_startDate"
                        useCurrent={false}
                        sideBySide
                        onHide={(e: any) => {
                            //props.setFieldValue("startDate", e.date ? (e.date as Moment).toISOString() : "");
                        }} />
                </div>
                <div className="form-group mr-2">
                    <label htmlFor="nrf52Query_endDate" className="custom-form-label">End Date</label>
                    <DateTimePicker 
                        buttons={{showClear: true}}
                        id="nrf52Query_endDate"
                        useCurrent={false}
                        sideBySide
                        onHide={(e: any) => {
                            props.setFieldValue("endDate", e.date ? (e.date as Moment).toISOString() : "");
                        }} />
                </div>
                <div className="form-group mr-2">
                    <label htmlFor="nrf52Query_owner" className="custom-form-label">User</label>
                    <input type="text"
                        className="form-control"
                        id="nrf52Query_owner"
                        name="owner"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur} />
                </div>
                <div className="form-group">
                    <label htmlFor="nrf52Query_deviceName" className="custom-form-label">Device Name</label>
                    <input type="text"
                        className="form-control"
                        id="nrf52Query_deviceName"
                        name="deviceName"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur} />
                </div>
            </div>
        </div>

        <div className="d-flex flex-row-reverse mb-2">
            <div>
                <button className="btn btn-primary ml-2" type="submit">Apply</button>
                <button className="btn btn-light ml-2" type="submit">Refresh</button>
                <div className="btn-group ml-2">
                    <button className="btn btn-light dropdown-toggle" data-toggle="dropdown">
                        Actions
                    </button>
                    <div className="dropdown-menu">
                        <button className="dropdown-item" onClick={e => {
                            e.preventDefault();

                            props.handleDownloadCSV();
                        }}>Export to CSV</button>
                    </div>
                </div>
            </div>
        </div>
    </form>);
}

export default QueryPanel;