import React from "react";
import Popup from "reactjs-popup";
import { sensorDataResolver } from "../utils";
import "./SensorDataResolver.css";


interface SensorDataResolverProps {
    data: string
}

function SensorDataResolver(props: SensorDataResolverProps) {
    if (props.data.length !== 128) {
        return <div>{props.data}</div>;
    }

    const resolved = sensorDataResolver(props.data);

    return (
        <Popup
            trigger={<div className="sensor-data-field">{props.data}</div>}
            position={["top center", "bottom center"]}
            on="hover"
            keepTooltipInside>
            <div className="container text-nowrap">
                <div className="row row-cols-5">
                    <div className="col p-0">{`SN: ${resolved.serialNumber}`}</div>
                    <div className="col-8 p-0">{`TS: ${resolved.timestamp.toLocaleString()}`}</div>
                </div>
                <div className="row row-cols-5">
                    <div className="col p-0">{`B: ${resolved.batteryLevel.toFixed(2)}V`}</div>
                    <div className="col p-0">{`SC: ${resolved.stepCount}`}</div>
                    <div className="col-5 p-0">{`Type: ${resolved.type}`}</div>
                </div>

                <div className="row mt-1 font-weight-bold" style={{fontSize: "smaller"}}>Temperature Sensors</div>
                <div className="row row-cols-5">
                    {[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17].map(val => <div className="col p-0" key={val}>
                        {`T${val}: ${resolved.temperatureSensors[val].toFixed(2)} °C`}
                    </div>)}
                </div>

                <div className="row mt-1 font-weight-bold" style={{fontSize: "smaller"}}>Load Sensors</div>
                <div className="row row-cols-5">
                    {[1,2,3,4,5].map(val => <div className="col p-0" key={val}>
                        {`L${val}: ${resolved.loadSensors[val - 1]}`}
                    </div>)}
                </div>

                <div className="row mt-1 font-weight-bold" style={{fontSize: "smaller"}}>IMU</div>
                <div className="row row-cols-5">
                    <div className="col p-0">{`x: ${resolved.IMU.x.toPrecision(3)}G`}</div>
                    <div className="col p-0">{`y: ${resolved.IMU.y.toPrecision(3)}G`}</div>
                    <div className="col p-0">{`z: ${resolved.IMU.z.toPrecision(3)}G`}</div>
                </div>
            </div>
        </Popup>
    );
}

export default SensorDataResolver;