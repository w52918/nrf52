import React, { useEffect, useState } from "react";
import { AuthState, onAuthUIStateChange } from "@aws-amplify/ui-components";
import { AmplifyAuthenticator, AmplifySignIn } from "@aws-amplify/ui-react";
import { getUserGroup } from "../utils";
import { UserContext } from "../contexts";
import { Auth } from "aws-amplify";
import "./MyAuth.css"

interface AuthProps {
    children?: React.ReactNode
}

function MyAuth(props: AuthProps) {
    const [authState, setAuthState] = useState<AuthState>();
    const [user, setUser] = useState<object | undefined>();

    useEffect(() => {
        return onAuthUIStateChange((nextAuthState, authData) => {
            // 登陆完成后检测用户权限
            if(nextAuthState === AuthState.SignedIn) {
                // 只允许管理员登陆
                if (authData && !getUserGroup(authData)?.includes("Admin")) {
                    Auth.signOut();
                    return;
                }
            }

            setAuthState(nextAuthState);
            setUser(authData);
        });
    }, []);

    return authState === AuthState.SignedIn && user ? (
        <UserContext.Provider value={user}>
            {props.children}
        </UserContext.Provider>
    ) : (<div className="d-flex justify-content-center align-items-center vh-100">
        <AmplifyAuthenticator usernameAlias="username">
            <AmplifySignIn slot="sign-in"
                usernameAlias="username"
                hideSignUp={true} />
        </AmplifyAuthenticator>
    </div>);
}

export default MyAuth;
