import React, { useEffect, useState } from "react";
import Nrf52Table from "./Nrf52Table";
import "./Nrf52Query.css";
import { query } from "../utils";
import { Formik } from "formik";
import Pagination from "./Pagination";
import QueryPanel from "./QueryPanel";
import { queryAndExportToCSV } from "../utils";

interface QueryState {
    startDateTime?: Date,
    endDateTime?: Date,
    deviceName?: string,
    sortDirection?: string,
    owner?: string,
    limit: number,
    nextToken?: string,
    pageIndex: number,
    cachedPageCount: number
}

const ITEMS_PER_PAGE = 50;

interface Nrf52QueryProps {

}

let cachedTableState = new Array<Array<{
    id: string
    owner: string,
    deviceName: string,
    sensorId: string,
    data: string,
    timestamp: Date,
}>>();

function Nrf52Query(props: Nrf52QueryProps) {
    const [tableState, setTableState] = useState(new Array<{
        id: string
        owner: string,
        deviceName: string,
        sensorId: string,
        data: string,
        timestamp: Date,
    }>());

    const [isInitQuerying, setInitQuerying] = useState(false);

    const [queryState, setQueryState] = useState<QueryState>({ pageIndex: 1, limit: ITEMS_PER_PAGE, cachedPageCount: 0 });

    useEffect(() => {
        setInitQuerying(true);
        queryAndSetTable({
            sortDirection: "DESC",
            limit: ITEMS_PER_PAGE
        }, setTableState, setQueryState, 0, 1)
            .finally(() => {
                setInitQuerying(false);
            });
    }, []);
    
    return (<Formik
        initialValues={{ startDate: "", endDate: "", deviceName: "", owner: "" }}
        validate={values => {

        }}
        onSubmit={(values, { setSubmitting }) => {
            queryAndSetTable({
                owner: values.owner || undefined,
                startDateTime: values.startDate ? new Date(values.startDate) : undefined,
                endDateTime: values.endDate ? new Date(values.endDate) : undefined,
                deviceName: values.deviceName,
                sortDirection: "DESC",
                limit: ITEMS_PER_PAGE
            }, setTableState, setQueryState, queryState.cachedPageCount, queryState.pageIndex)
                .finally(() => {
                    setSubmitting(false);
                });
        }}>
        {({ handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setSubmitting,
            setFieldValue }) => (<>
                <QueryPanel
                    handleChange={handleChange}
                    setFieldValue={setFieldValue}
                    handleBlur={handleBlur}
                    handleSubmit={handleSubmit}
                    handleDownloadCSV={() => queryAndExportToCSV({
                        startDateTime: queryState.startDateTime,
                        endDateTime: queryState.endDateTime,
                        deviceName: queryState.deviceName,
                        sortDirection: queryState.sortDirection,
                        owner: queryState.owner
                    })} />

                {isInitQuerying || isSubmitting ?
                    (<div className="d-flex justify-content-center">
                        <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>) :
                    (tableState.length > 0 || queryState.pageIndex > 1 ? (<>
                        <Nrf52Table
                            items={tableState}
                            startingIndex={queryState.limit * (queryState.pageIndex - 1) + 1} />
                        <Pagination
                            isFirstPage={queryState.pageIndex <= 1}
                            isLastPage={!queryState.nextToken && queryState.pageIndex >= queryState.cachedPageCount}
                            onPrevClick={() => {
                                if (queryState.pageIndex <= 1) {
                                    return;
                                }

                                setSubmitting(true);

                                setTableState(cachedTableState[queryState.pageIndex - 2]);
                                setQueryState({
                                    ...queryState,
                                    pageIndex: queryState.pageIndex - 1
                                });

                                setSubmitting(false);
                            }}
                            onNextClick={async () => {
                                if (!queryState.nextToken && queryState.pageIndex >= queryState.cachedPageCount) {
                                    return;
                                }

                                setSubmitting(true);

                                if (cachedTableState.length > queryState.pageIndex) {
                                    setTableState(cachedTableState[queryState.pageIndex]);
                                    setQueryState({
                                        ...queryState,
                                        pageIndex: queryState.pageIndex + 1
                                    });
                                    setSubmitting(false);
                                } else {
                                    await queryAndSetTable(queryState, setTableState, setQueryState, queryState.cachedPageCount, queryState.pageIndex);

                                    setSubmitting(false);
                                }
                            }}
                            onHomeClick={() => {
                                setSubmitting(true);

                                queryAndSetTable({ ...queryState, nextToken: undefined }, setTableState, setQueryState, 0, 1)
                                    .finally(() => {
                                        setSubmitting(false);
                                    });
                            }
                            } />
                    </>) : (<div className="row justify-content-center no-item">No Item</div>))}
            </>)}
    </Formik>);
}

async function queryAndSetTable(vars: {
    owner?: string,
    startDateTime?: Date,
    endDateTime?: Date,
    deviceName?: string,
    limit: number,
    sortDirection?: string,
    nextToken?: string
}, setTableState: (value: any) => void,
    setQueryState: (value: QueryState) => void,
    cachedPageCount: number,
    pageIndex: number) {
    const list = await query(vars);

    setTableState(list.data);
    setQueryState({
        ...vars,
        nextToken: list.nextToken,
        pageIndex: vars.nextToken ? pageIndex + 1 : 1,
        cachedPageCount: vars.nextToken ? cachedPageCount + 1 : 1
    });

    if (!vars.nextToken) {
        cachedTableState = [];
    }

    console.assert(cachedTableState.length === pageIndex - 1);
    cachedTableState.push(list.data);
}


export default Nrf52Query;