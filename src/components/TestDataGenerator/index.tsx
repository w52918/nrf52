import React from "react";
import "./index.css";
import DeviceGenerator from "./DeviceGenerator";
import DataGenerator from "./DataGenerator";
import ImportCSV from "./ImportCSV";


// 用于生成假数据
function TestDataGenerator() {
    return (<div className="card mt-2">
        <div className="card-header">TestDataGenerator</div>
        <div className="card-body">
            <DeviceGenerator />
            <DataGenerator />
            <ImportCSV />
        </div>
    </div>)
}


export default TestDataGenerator;
