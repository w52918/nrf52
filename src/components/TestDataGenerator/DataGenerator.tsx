import { GraphQLResult } from "@aws-amplify/api-graphql";
import { API, graphqlOperation } from "aws-amplify";
import { Formik } from "formik";
import React from "react";
import { GetDeviceBySensorIdQuery } from "../../API";
import { getDeviceBySensorId } from "../../graphql/queries";
import { addData } from "./common";

function DataGenerator() {
    return (<>
        <h5>Data</h5>
        <Formik
            initialValues={{ owner: "", sensorId: "" }}
            validate={values => {
                const errors: { sensorId?: string } = {};

                if (!values.sensorId) {
                    errors.sensorId = "Device ID is required.";
                }

                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                addRandomData({ sensorId: values.sensorId, count: 10, dataSize: 128, owner: values.owner || undefined})
                    .then(() => console.log("10 items added."))
                    .catch(e => console.error(e))
                    .finally(() => {
                        setSubmitting(false);
                    });
            }}>
            {({ errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => <>
                <form className="form-inline" onSubmit={handleSubmit}>
                    <label className="sr-only" htmlFor="dataGenerator_owner">User Name</label>
                    <input type="text"
                        id="dataGenerator_owner"
                        name="owner"
                        className="form-control mb-2 mr-sm-2"
                        placeholder="User Name"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    <label className="sr-only" htmlFor="dataGenerator_sensorId">Device ID</label>
                    <input type="text"
                        id="dataGenerator_sensorId"
                        name="sensorId"
                        className="form-control mb-2 mr-sm-2"
                        placeholder="Device ID"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    <button type="submit" className="btn btn-primary mb-2">{isSubmitting ? (<>
                        <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true" />
                        <span className="ml-1">Adding...</span>
                    </>) : "Add 10 random items"}</button>
                </form>
            </>}

        </Formik>
    </>)
}

// 在数据库中创建随机生成的数据
// deviceId: 设备 ID，不能为空
// count: 创建的条目数，必须大于0
// dataSize: 数据长度，必须大于0
async function addRandomData(vars: {sensorId: string, count: number, dataSize: number, owner?: string}) {
    console.assert(vars.sensorId && vars.count > 0 && vars.dataSize > 0);

    const result = await API.graphql(graphqlOperation(getDeviceBySensorId, {
        sensorId: vars.sensorId,
        limit: 1
    })) as GraphQLResult<GetDeviceBySensorIdQuery>;

    if(!result.data?.getDeviceBySensorId?.items?.length) {
        throw new Error("Invalid sensorId");
    }

    await Promise.all(new Array(vars.count).fill(0).map(() => {
        return addData({
            deviceId: result.data!.getDeviceBySensorId!.items![0]!.id,
            data: genRandomData(vars.dataSize),
            owner: vars.owner
        });
    }));
}

// 生成随机数据
function genRandomData(len: number) {
    let data = "";
    for (let i = 0; i < len; ++i) {
        data += Math.floor(Math.random() * 16).toString(16);
    }
    return data;
}

export default DataGenerator;