import React from "react";
import { Formik } from "formik";
import { addDevice } from "./common";

function DeviceGenerator() {
    return (<>
        <h5>Device</h5>
        <Formik
            initialValues={{ owner: "", sensorId: "", name: "" }}
            validate={values => {
                const errors: { sensorId?: string, name?: string } = {};

                if (!values.sensorId) {
                    errors.sensorId = "Device ID is required";
                }
                if (!values.name) {
                    errors.name = "Device Name is required";
                }

                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                addDevice({ sensorId: values.sensorId, name: values.name, owner: values.owner || undefined })
                    .then(result => console.log(`A device(ID: ${result.createDevice?.id}) has been created.`))
                    .catch(e => console.error(e))
                    .finally(() => {
                        setSubmitting(false);
                    });
            }}>
            {({ errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (<>
                <form className="form-inline" onSubmit={handleSubmit}>
                    <label className="sr-only" htmlFor="deviceDataGenerator_owner">User Name</label>
                    <input type="text"
                        id="deviceDataGenerator_owner"
                        name="owner"
                        className="form-control mb-2 mr-sm-2"
                        placeholder="User Name"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    <label className="sr-only" htmlFor="deviceDataGenerator_sensorId">Device ID</label>
                    <input type="text"
                        id="deviceDataGenerator_sensorId"
                        name="sensorId"
                        className="form-control mb-2 mr-sm-2"
                        placeholder="Device ID"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    <label className="sr-only" htmlFor="deviceDataGenerator_name">Device Name</label>
                    <input type="text"
                        className="form-control mb-2 mr-sm-2"
                        id="deviceDataGenerator_name"
                        name="name"
                        placeholder="Device Name"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    <button type="submit"
                        className="btn btn-primary mb-2"
                        disabled={isSubmitting}>{isSubmitting ? (<>
                            <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true" />
                            <span className="ml-1">Adding</span>
                        </>) : "Add"}</button>
                </form>
            </>)}
        </Formik>
    </>)
}


export default DeviceGenerator;