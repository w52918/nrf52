import { GraphQLResult } from "@aws-amplify/api-graphql";
import { API, graphqlOperation } from "aws-amplify";
import { CreateDeviceMutation, CreateRpiNrf52Mutation } from "../../API";
import { createDevice, createRpiNrf52 } from "../../graphql/mutations";

// 在数据库中创建 Device
// id 和 name 不能为空
export async function addDevice(vars: {sensorId: string, name: string, owner?: string}) {
    console.assert(vars.sensorId && vars.name);

    const result = await API.graphql(graphqlOperation(createDevice, {
        input: vars
    })) as GraphQLResult<CreateDeviceMutation>;

    if(result.errors || !result.data) {
        throw new Error("");
    }

    return result.data;
}

export async function addData(vars: {deviceId: string, data: string, owner?: string}) {
    return API.graphql(graphqlOperation(createRpiNrf52, {
        input: { ...vars, sortHelper: 0}
    })) as Promise<GraphQLResult<CreateRpiNrf52Mutation>>;
}
