import { Formik } from "formik";
import Papa from "papaparse";
import React, { createRef } from "react";
import { addData, addDevice } from "./common";

function ImportCSV() {
    const fileInputRef = createRef<HTMLInputElement>();

    return (<>
        <h5>From CSV</h5>
        <Formik
            initialValues={{ fileName: "" }}
            onSubmit={(_values, { setSubmitting }) => {
                if (fileInputRef.current?.files && fileInputRef.current.files[0]) {
                    setSubmitting(true);

                    Papa.parse<{
                        user_name: string,
                        device_name: string,
                        device_id: string,
                        data: string
                    }>(fileInputRef.current.files[0], {
                        header: true,
                        complete: async result => {
                            if (!result.data) {
                                setSubmitting(false);
                                return;
                            }

                            // Remove the last item if it is empty
                            if (!result.data[result.data.length - 1].device_id) {
                                result.data.pop();
                            }

                            // Find out all devices
                            const deviceMap = new Map<string, {id?: string, name: string, owner: string}>();
                            for (const item of result.data) {
                                if (!deviceMap.has(item.device_id)) {
                                    deviceMap.set(item.device_id, {name: item.device_name, owner: item.user_name});
                                }
                            }
                            
                            // Upload devices
                            console.log("devices");
                            const promises = new Array<Promise<void>>();
                            deviceMap.forEach((val, key) => {
                                promises.push(addDevice({sensorId: key, ...val}).then(result => {
                                    val.id = result.createDevice?.id;
                                }));
                            });
                            
                            await Promise.all(promises);
                            // Upload datas
                            console.log("datas");
                            const templateData = "00015e58Cf89ff011170117011701170117011701170117011701170117011701170117011701170117011700800080008000800080001020102100207500001";

                            await Promise.all(result.data.map(val => { 
                                return addData({
                                deviceId: deviceMap.get(val.device_id)?.id || "",
                                data: val.data + templateData.substr(val.data.length),
                                owner: val.user_name
                            });}));

                            setSubmitting(false);
                        },
                        error: err => {
                            console.error(err);
                            setSubmitting(false);
                        }
                    });
                }
            }}>
            {({ values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                <form className="form-inline" onSubmit={handleSubmit}>
                    <div className="mb-2 mr-sm-2 col-5 pl-0 pr-0">
                        <div className="custom-file">
                            <input type="file"
                                accept=".csv"
                                className="custom-file-input"
                                name="fileName"
                                id="testDataGenerator_file"
                                ref={fileInputRef}
                                onChange={handleChange}
                                onBlur={handleBlur} />
                            <label className="custom-file-label" htmlFor="testDataGenerator_file">
                                {values.fileName || "Choose file"}
                            </label>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary mb-2">
                        {isSubmitting ? (<>
                            <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <span className="ml-1">Importing...</span>
                        </>) : "Import"}</button>
                </form>
            )}
        </Formik>
    </>)
}

export default ImportCSV;