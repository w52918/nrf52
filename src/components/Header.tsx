import { Auth } from "aws-amplify";
import React from "react";
import { UserContext } from "../contexts";

function Header() {
    return (<UserContext.Consumer>
        {user => <div className="d-block border-bottom d-flex flex-row-reverse">
            <div className="p-2">
                <div className="d-inline align-middle mr-2">Welcome, {user.username}</div>
                <button className="btn btn-secondary" onClick={() => {
                    Auth.signOut().catch(e => console.error(e));
                }}>Log out</button>
            </div>
        </div>}
    </UserContext.Consumer>);
}

export default Header;
