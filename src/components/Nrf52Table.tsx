import React from "react";
import "./Nrf52Table.css";
import SensorDataResolver from "./SensorDataResolver";

interface Nrf52TableProps {
    items: {
        id: string,
        owner: string,
        deviceName: string,
        sensorId: string,
        data: string,
        timestamp: Date
    }[],
    startingIndex: number
}

function Nrf52Table(props: Nrf52TableProps) {
    return (<table className="table table-striped table-responsive-md">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Device Name</th>
                <th scope="col">Device ID</th>
                <th scope="col">Data</th>
                <th scope="col">Timestamp</th>
            </tr>
        </thead>
        <tbody>
            {props.items.map((val, idx) => <tr key={val.id}>
                <td>{props.startingIndex + idx}</td>
                <td>{val.owner}</td>
                <td>{val.deviceName}</td>
                <td>{val.sensorId}</td>
                <td><SensorDataResolver data={val.data} /></td>
                <td>{val.timestamp.toLocaleString()}</td>
            </tr>)}
        </tbody>
    </table>);
};

export default Nrf52Table;