/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getRpiNrf52 = /* GraphQL */ `
  query GetRpiNrf52($id: ID!) {
    getRpiNrf52(id: $id) {
      id
      device {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      sortHelper
      updatedAt
    }
  }
`;
export const listRpiNrf52s = /* GraphQL */ `
  query ListRpiNrf52s(
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    listRpiNrf52s(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        device {
          id
          sensorId
          name
          owner
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        sortHelper
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDevice = /* GraphQL */ `
  query GetDevice($id: ID!) {
    getDevice(id: $id) {
      id
      sensorId
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          sortHelper
          updatedAt
        }
        nextToken
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const listDevices = /* GraphQL */ `
  query ListDevices(
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDevices(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRpiNrf52ByDeviceId = /* GraphQL */ `
  query GetRpiNrf52ByDeviceId(
    $deviceId: ID
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    getRpiNrf52ByDeviceId(
      deviceId: $deviceId
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        device {
          id
          sensorId
          name
          owner
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        sortHelper
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRpiNrf52ByOwner = /* GraphQL */ `
  query GetRpiNrf52ByOwner(
    $owner: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    getRpiNrf52ByOwner(
      owner: $owner
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        device {
          id
          sensorId
          name
          owner
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        sortHelper
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRpiNrf52ByCreatedAt = /* GraphQL */ `
  query GetRpiNrf52ByCreatedAt(
    $sortHelper: Int
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelRpiNrf52FilterInput
    $limit: Int
    $nextToken: String
  ) {
    getRpiNrf52ByCreatedAt(
      sortHelper: $sortHelper
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        device {
          id
          sensorId
          name
          owner
          createdAt
          updatedAt
        }
        deviceId
        data
        createdAt
        owner
        sortHelper
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDeviceByName = /* GraphQL */ `
  query GetDeviceByName(
    $name: String
    $owner: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getDeviceByName(
      name: $name
      owner: $owner
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDeviceByOwner = /* GraphQL */ `
  query GetDeviceByOwner(
    $owner: String
    $name: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getDeviceByOwner(
      owner: $owner
      name: $name
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDeviceBySensorId = /* GraphQL */ `
  query GetDeviceBySensorId(
    $sensorId: String
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getDeviceBySensorId(
      sensorId: $sensorId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
