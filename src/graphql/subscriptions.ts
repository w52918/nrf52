/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateRpiNrf52 = /* GraphQL */ `
  subscription OnCreateRpiNrf52($owner: String) {
    onCreateRpiNrf52(owner: $owner) {
      id
      device {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      sortHelper
      updatedAt
    }
  }
`;
export const onUpdateRpiNrf52 = /* GraphQL */ `
  subscription OnUpdateRpiNrf52($owner: String) {
    onUpdateRpiNrf52(owner: $owner) {
      id
      device {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      sortHelper
      updatedAt
    }
  }
`;
export const onDeleteRpiNrf52 = /* GraphQL */ `
  subscription OnDeleteRpiNrf52($owner: String) {
    onDeleteRpiNrf52(owner: $owner) {
      id
      device {
        id
        sensorId
        name
        rpiNrf52s {
          nextToken
        }
        owner
        createdAt
        updatedAt
      }
      deviceId
      data
      createdAt
      owner
      sortHelper
      updatedAt
    }
  }
`;
export const onCreateDevice = /* GraphQL */ `
  subscription OnCreateDevice($owner: String) {
    onCreateDevice(owner: $owner) {
      id
      sensorId
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          sortHelper
          updatedAt
        }
        nextToken
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDevice = /* GraphQL */ `
  subscription OnUpdateDevice($owner: String) {
    onUpdateDevice(owner: $owner) {
      id
      sensorId
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          sortHelper
          updatedAt
        }
        nextToken
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDevice = /* GraphQL */ `
  subscription OnDeleteDevice($owner: String) {
    onDeleteDevice(owner: $owner) {
      id
      sensorId
      name
      rpiNrf52s {
        items {
          id
          deviceId
          data
          createdAt
          owner
          sortHelper
          updatedAt
        }
        nextToken
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
