export function getUserGroup(user: any): Array<string> {
    return user?.signInUserSession?.accessToken?.payload["cognito:groups"] || [];
}
