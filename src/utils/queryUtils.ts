import { GraphQLResult } from "@aws-amplify/api-graphql";
import { API, graphqlOperation } from "aws-amplify";
import Papa from "papaparse";
import streamSaver from "streamsaver";
import { GetRpiNrf52ByCreatedAtQuery, GetRpiNrf52ByOwnerQuery } from "../API";
import { getRpiNrf52ByCreatedAt, getRpiNrf52ByOwner } from "../graphql/queries";


// 查询数据库
export async function query(vars: {
    owner?: string,
    startDateTime?: Date,
    endDateTime?: Date,
    deviceName?: string,
    limit?: number,
    sortDirection?: string,
    nextToken?: string
}): Promise<{
    data: {
        id: string,
        owner: string,
        sensorId: string,
        deviceName: string,
        data: string,
        timestamp: Date
    }[],
    nextToken?: string
}> {
    if (vars.limit !== undefined && vars.limit <= 0) {
        console.error("Invalid query.");
        return { data: [] };
    }

    if (vars.startDateTime && vars.endDateTime && vars.startDateTime >= vars.endDateTime) {
        return { data: [] };
    }

    const createdAt = _createdAtCondition(vars.startDateTime, vars.endDateTime);

    if (vars.deviceName) {
        return _queryByDeviceName({
            owner: vars.owner,
            createdAt,
            deviceName: vars.deviceName,
            limit: vars.limit,
            sortDirection: vars.sortDirection,
            nextToken: vars.nextToken
        });
    } else if (vars.owner) {
        return _queryByOwner({
            owner: vars.owner,
            createdAt,
            limit: vars.limit,
            sortDirection: vars.sortDirection,
            nextToken: vars.nextToken
        });
    } else {
        return _queryByCreatedAt({
            createdAt,
            limit: vars.limit,
            sortDirection: vars.sortDirection,
            nextToken: vars.nextToken
        });
    }
}

function _createdAtCondition(startDateTime?: Date, endDateTime?: Date) {
    const startDateTimeISO = startDateTime?.toISOString();
    const endDateTimeISO = endDateTime?.toISOString();

    if (startDateTimeISO) {
        if (endDateTimeISO) {
            return {
                between: [startDateTimeISO, endDateTimeISO]
            };
        } else {
            return {
                ge: startDateTimeISO
            }
        }
    } else {
        if (endDateTimeISO) {
            return {
                le: endDateTimeISO
            }
        } else {
            return undefined; // No createdAt condition
        }
    }
}

async function _queryByDeviceName(vars: {
    owner?: string,
    createdAt?: object,
    deviceName: string,
    limit?: number,
    sortDirection?: string,
    nextToken?: string
}): Promise<{
    data: {
        id: string,
        owner: string,
        sensorId: string,
        deviceName: string,
        data: string,
        timestamp: Date
    }[],
    nextToken?: string
}> {
    const queryStr = `
        query GetDeviceByName(
            $name: String
            $sortDirection: ModelSortDirection
            $filter: ModelRpiNrf52FilterInput
            $limit: Int
            $createdAt: ModelStringKeyConditionInput
            $nextToken: String
        ) {
            getDeviceByName(
                name: $name
                limit: 1000
            ) {
                items {
                    id
                    sensorId
                    name
                    rpiNrf52s(
                        sortDirection: $sortDirection
                        filter: $filter
                        limit: $limit
                        createdAt: $createdAt
                        nextToken: $nextToken
                    ) {
                        items {
                            id
                            data,
                            createdAt,
                            owner
                        }
                        nextToken
                    }
                    createdAt
                    updatedAt
                }
            }
        }`

    const result = await API.graphql(
        graphqlOperation(queryStr, {
            name: vars.deviceName,
            sortDirection: vars.sortDirection,
            limit: vars.limit,
            createdAt: vars.createdAt,
            nextToken: vars.nextToken,
            filter: vars.owner ? { owner: { eq: vars.owner } } : undefined
        })) as GraphQLResult<any>;
    if (result.data?.getDeviceByName?.items && result.data.getDeviceByName.items[0]) {
        const device = result.data.getDeviceByName.items[0];

        return {
            data: device.rpiNrf52s.items.map((item: any) => {
                return {
                    id: item.id || "",
                    owner: item.owner || "",
                    sensorId: device.sensorId || "",
                    deviceName: vars.deviceName || "",
                    data: item.data || "",
                    timestamp: item.createdAt ? new Date(item.createdAt) : new Date()
                };
            }), nextToken: device.rpiNrf52s?.nextToken || undefined
        };
    } else {
        return { data: [] };
    }
}

async function _queryByOwner(vars: {
    owner: string,
    createdAt?: object,
    limit?: number,
    sortDirection?: string,
    nextToken?: string
}): Promise<{
    data: {
        id: string,
        owner: string,
        sensorId: string,
        deviceName: string,
        data: string,
        timestamp: Date
    }[],
    nextToken?: string
}> {
    const result = await API.graphql(graphqlOperation(getRpiNrf52ByOwner, vars)) as GraphQLResult<GetRpiNrf52ByOwnerQuery>;

    if (result.errors) {
        throw new Error();
    }

    if (!result.data?.getRpiNrf52ByOwner?.items) {
        return { data: [] };
    }

    return {
        data: result.data.getRpiNrf52ByOwner.items.map(val => {
            return {
                id: val?.id || "",
                owner: val?.owner || "",
                sensorId: val?.device?.sensorId || "",
                deviceName: val?.device?.name || "",
                data: val?.data || "",
                timestamp: val?.createdAt ? new Date(val.createdAt) : new Date()
            };
        }),
        nextToken: result.data.getRpiNrf52ByOwner.nextToken || undefined
    };
}

async function _queryByCreatedAt(vars: {
    createdAt?: object,
    limit?: number,
    sortDirection?: string,
    nextToken?: string
}): Promise<{
    data: {
        id: string,
        owner: string,
        sensorId: string,
        deviceName: string,
        data: string,
        timestamp: Date
    }[],
    nextToken?: string
}> {
    const result = await API.graphql(graphqlOperation(getRpiNrf52ByCreatedAt, {
        ...vars,
        sortHelper: 0
    })) as GraphQLResult<GetRpiNrf52ByCreatedAtQuery>;

    if (result.errors) {
        throw new Error();
    }

    if (!result.data?.getRpiNrf52ByCreatedAt?.items) {
        return { data: [] };
    }

    return {
        data: result.data.getRpiNrf52ByCreatedAt.items.map(val => {
            return {
                id: val?.id || "",
                owner: val?.owner || "",
                sensorId: val?.device?.sensorId || "",
                deviceName: val?.device?.name || "",
                data: val?.data || "",
                timestamp: val?.createdAt ? new Date(val.createdAt) : new Date()
            };
        }), nextToken: result.data.getRpiNrf52ByCreatedAt.nextToken || undefined
    };
}


// Export query result to CSV file
export async function queryAndExportToCSV(queryVars: {
    owner?: string,
    userName?: string,
    startDateTime?: Date,
    endDateTime?: Date,
    deviceName?: string,
    sortDirection?: string
}, fileName = "data.csv") {
    const stream = streamSaver.createWriteStream(fileName);
    const writer = stream.getWriter();

    let result = await query({
        ...queryVars,
        limit: 1000
    });

    let data = result.data.map(val => {
        return {
            user_name: val.owner,
            device_name: val.deviceName,
            device_id: val.sensorId,
            data: val.data
        };
    });

    const encoder = new TextEncoder();
    await writer.write(encoder.encode(Papa.unparse(data, { header: true })));

    while (result.nextToken) {
        result = await query({
            ...queryVars,
            nextToken: result.nextToken,
            limit: 1000
        });

        data = result.data.map(val => {
            return {
                user_name: val.owner,
                device_name: val.deviceName,
                device_id: val.sensorId,
                data: val.data
            };
        });
        await writer.write(encoder.encode(Papa.unparse(data, { header: false })));
    }

    await writer.close();
}
