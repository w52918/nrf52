const CONSTANTS = {
    TEMPERATURE: 0.00390625,
    IMU: 0.0002241,
    BATTERY: 0.001763,
    STEPCOUNT: 1.0
};

const TYPES = ["Sensor board data", "Ambient sensor data", "Time synchronization request", "Reserved for sending"];

interface sensorData {
    serialNumber: number,
    timestamp: Date,
    type: string,
    temperatureSensors: number[],
    loadSensors: number[],
    IMU: {
        x: number,
        y: number,
        z: number
    },
    batteryLevel: number,
    stepCount: number
}

export function sensorDataResolver(rawData: string): sensorData {
    if(rawData.length !== 128) {
        throw new Error();
    }
    
    const serialNumber = Number.parseInt(rawData.substring(0, 4), 16);
    const timestamp = new Date(Number.parseInt(rawData.substring(4, 12), 16) * 1000);

    // Type
    const typeId = Number.parseInt(rawData.substring(14, 16), 16);
    let type: string;
    if(typeId === 1) {
        type = TYPES[0];
    } else if (typeId === 2) {
        type = TYPES[1];
    } else if (typeId === 3) {
        type = TYPES[2];
    } else if (typeId >= 4 && typeId <= 15) {
        type = TYPES[3];
    } else {
        type = "unkown";
    }

    // Temperature sensors
    const temperatureSensors = new Array<number>();
    for(let i = 0; i < 18; ++i) {
        const temp = rawData.substr(16 + 4 * i, 4);
        temperatureSensors.push(Number.parseInt(temp, 16) * CONSTANTS.TEMPERATURE);
    }

    // Load sensors
    const loadSensors = new Array<number>();
    for(let i = 0; i < 5; ++i) {
        const load = rawData.substr(88 + 4 * i, 4);
        loadSensors.push(Number.parseInt(load, 16));
    }

    // IMU
    const parseToSigned = (data: string) => {
        console.assert(data.length === 4);

        const rt = Number.parseInt(data, 16);
        return rt >= 32768 ? 32768 - rt : rt;
    };

    const IMU = {
        x: parseToSigned(rawData.substr(108, 4)) * CONSTANTS.IMU,
        y: parseToSigned(rawData.substr(112, 4)) * CONSTANTS.IMU,
        z: parseToSigned(rawData.substr(116, 4)) * CONSTANTS.IMU
    };

    const batteryLevel = Number.parseInt(rawData.substr(120, 4), 16) * CONSTANTS.BATTERY;
    const stepCount = Number.parseInt(rawData.substr(124, 4), 16) * CONSTANTS.STEPCOUNT;

    return {
        serialNumber,
        timestamp,
        type,
        temperatureSensors,
        loadSensors,
        IMU,
        batteryLevel,
        stepCount
    };
}