import React from 'react';
import './App.css';
import Nrf52Query from './components/Nrf52Query';
import MyAuth from './components/MyAuth';
import Header from './components/Header';


function App() {
    return (<MyAuth>
        <Header />
        <div className="container">
            <Nrf52Query />
        </div>
    </MyAuth>);
}

export default App;
